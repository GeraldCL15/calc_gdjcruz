﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form

    {
        private double valor1;
        private double valor2;
        private double resultado;
        private int bandera; 
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn0_Click(object sender, EventArgs e)
        {
            // numero 0
            txt_Display.Text = txt_Display.Text + "0";

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            //numero 1
            txt_Display.Text = txt_Display.Text + "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            // numero 2
            txt_Display.Text = txt_Display.Text + "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            //numero 3
            txt_Display.Text = txt_Display.Text + "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            //numero 4
            txt_Display.Text = txt_Display.Text +"4";
        }

        private void btn5_Click(object sender, EventArgs e)
       {
            //numero 5
            txt_Display.Text = txt_Display.Text +"5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            //numero 6
            txt_Display.Text = txt_Display.Text + "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            //numero 7
            txt_Display.Text = txt_Display.Text + "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            //numero 8
            txt_Display.Text = txt_Display.Text+"8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            //numero 9
            txt_Display.Text = txt_Display.Text+"9";
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            //borrar 
            txt_Display.Text = " ";
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            //resultado
            valor2 = Convert.ToDouble(txt_Display.Text);

            switch (bandera)
            {

                case 1:
                    resultado = valor1 + valor2; break;

                case 2:
                    resultado = valor1 - valor2; break;

                case 3:
                    resultado = valor1 * valor2; break;

                case 4:
                    resultado = valor1 / valor2; break;


            }

            txt_Display.Text = resultado.ToString();


        }

        private void btnSuma_Click(object sender, EventArgs e)
        {
            //boton suma
            bandera = 1;
            valor1 = Convert.ToDouble(txt_Display.Text);
            txt_Display.Text = "";
        }

        private void btnResta_Click(object sender, EventArgs e)
        {
            //boton resta
            bandera = 2;
            valor1 = Convert.ToDouble(txt_Display.Text);
            txt_Display.Text="";
        }

        private void btnMultiplicacion_Click(object sender, EventArgs e)
        {
            //Boton de multiplicación
            bandera = 3;
            valor1 = Convert.ToDouble(txt_Display.Text);
            txt_Display.Text = "";
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            //Boton de divición 
            bandera = 4;
            valor1 = Convert.ToDouble(txt_Display.Text);
            txt_Display.Text = "";
        }

        private void btnPunto_Click(object sender, EventArgs e)
        {
            txt_Display.Text = txt_Display.Text + ".";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double factorial = 1;
           valor1 = Convert.ToDouble(txt_Display.Text);
            for(double i= valor1; i > 0; i--)
            {
                factorial = factorial * i;
            }

            txt_Display.Text = factorial.ToString();

        }

        private void button21_Click(object sender, EventArgs e)
        {            
            Form2 newForm = new Form2();            
            newForm.Show();

            newForm.getTextHandler = newForm.getValue;
            newForm.getTextHandler(txt_Display.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            valor1 = Convert.ToDouble(txt_Display.Text);
            double a= 0;
            
            for (double i=1; i < (valor1+1); i++)
            {
                if (valor1 % i ==0)
                {
                    a++;
                }

            }

            if (a != 2)
            {
                txt_Display.Text = "No es un numero primo";

            }
            else
            {
                txt_Display.Text = "Si  es un numero primo";
            }


        }

        private void Clean_Buttom_Click(object sender, EventArgs e)
        {
            String texto= txt_Display.Text;
            String textoR= texto.Substring(0,txt_Display.Text.Length-1);
            txt_Display.Clear();
            txt_Display.Text = textoR;


        }
    }
}
