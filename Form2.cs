﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public delegate void GetTextHandler(string text);
        public GetTextHandler getTextHandler;

        public void getValue(string text)
        {
            // Serie Fibonacci
            int a = 0;
            int b = 1;
            int limite;
            int aux;

            limite = int.Parse(text);

            for (int i = 0; i < limite; i++) {
                aux = a;
                a = b;
                b = aux + a;
                listBox1.Items.Add(a);
            }
            txt_Fibonacci.Text = Convert.ToString(a);
        }
    }
}
